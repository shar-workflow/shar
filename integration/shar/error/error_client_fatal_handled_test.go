package error

import (
	"context"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/shar-workflow/shar/client/task"
	support "gitlab.com/shar-workflow/shar/internal/integration-support"
	"gitlab.com/shar-workflow/shar/server/messages"
	"log/slog"
	"os"
	"testing"
	"time"

	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/shar-workflow/shar/client"
	"gitlab.com/shar-workflow/shar/model"
)

type executionParams struct {
	startVars        model.Vars
	expectingFailure bool
}

func TestFatalErrorPersistedWithPauseHandlingStrategy(t *testing.T) {
	t.Parallel()

	// Create a starting context
	ctx := context.Background()

	// Dial shar
	ns := ksuid.New().String()
	cl := client.New(client.WithEphemeralStorage(), client.WithConcurrency(10), client.WithNamespace(ns))
	err := cl.Dial(ctx, tst.NatsURL)
	require.NoError(t, err)

	execParams := []executionParams{
		{startVars: model.NewVars(), expectingFailure: true},
		{startVars: model.NewVars(), expectingFailure: true},
		{startVars: model.NewVars(), expectingFailure: true},
	}

	fatalErrorHandlerDef := fatalErrorHandledHandlerDef{test: t, fatalErr: make(chan *model.FatalError)}
	wfName := "TestHandleFatalError"
	workflowId, executionIds, _ := runExecutions(ctx, fatalErrorHandlerDef, cl, ns, execParams, bpmnDetails{
		processId: "SimpleProcess",
		bpmnFile:  "../../../testdata/simple-workflow.bpmn",
		wfName:    wfName,
	}, map[string]task.ServiceFn{"../simple/simple_test.yaml": willPanicAndCauseWorkflowFatalError})

	assert.Eventually(t, func() bool {
		fatalErrors, err := cl.GetFatalErrors(ctx, wfName, workflowId, "", "")
		require.NoError(t, err)
		return len(fatalErrors) == len(executionIds)
	}, 3*time.Second, 100*time.Millisecond)

	for _, executionId := range executionIds {
		assert.Eventually(t, func() bool {
			fatalErrors, err := cl.GetFatalErrors(ctx, wfName, workflowId, executionId, "")
			require.NoError(t, err)
			return len(fatalErrors) == 1 && fatalErrors[0].WorkflowState.ExecutionId == executionId
		}, 3*time.Second, 100*time.Millisecond)
	}

	assert.Eventually(t, func() bool {
		fatalErrors, err := cl.GetFatalErrors(ctx, wfName, workflowId, "*", "*")
		require.NoError(t, err)
		return len(fatalErrors) == len(executionIds)
	}, 3*time.Second, 100*time.Millisecond)
}

func getWorkflowIdFrom(t *testing.T, ctx context.Context, cl *client.Client, name string) string {
	versions, err := cl.GetWorkflowVersions(ctx, name)
	require.NoError(t, err)
	require.Len(t, versions, 1) //should only be 1 version
	latestVersion := versions[len(versions)-1]
	return latestVersion.Id
}

func TestRetryFatalErroredProcess(t *testing.T) {
	t.Parallel()

	// Create a starting context
	ctx := context.Background()

	// Dial shar
	ns := ksuid.New().String()
	cl := client.New(client.WithEphemeralStorage(), client.WithConcurrency(10), client.WithNamespace(ns))
	err := cl.Dial(ctx, tst.NatsURL)
	require.NoError(t, err)

	pId1 := int64(1)
	pId2 := int64(2)

	newVars1 := model.NewVars()
	newVars1.SetBool("blowUp", false)
	newVars1.SetInt64("pId", int64(pId1))

	newVars2 := model.NewVars()
	newVars2.SetBool("blowUp", true)
	newVars2.SetInt64("pId", int64(pId2))

	execParams := []executionParams{
		{startVars: newVars1, expectingFailure: false},
		{startVars: newVars2, expectingFailure: true},
	}

	fatalErrorHandlerDef := fatalErrorHandledHandlerDef{test: t, fatalErr: make(chan *model.FatalError)}
	wfName := "TestHandleFatalError"
	workflowId, executionIds, completionChan := runExecutions(ctx, fatalErrorHandlerDef, cl, ns, execParams, bpmnDetails{
		processId: "SimpleProcess",
		bpmnFile:  "../../../testdata/simple-workflow.bpmn",
		wfName:    wfName,
	}, map[string]task.ServiceFn{"../simple/simple_test.yaml": willConditionallyPanicDependingOnVar})

	assert.Eventually(t, func() bool {
		successExecutionId := executionIds[0]
		fatalErrors, err := cl.GetFatalErrors(ctx, wfName, workflowId, successExecutionId, "")
		require.NoError(t, err)
		return len(fatalErrors) == 0
	}, 3*time.Second, 100*time.Millisecond)

	var fatalErrors []*model.FatalError
	failedExecutionId := executionIds[1]
	require.Eventually(t, func() bool {
		fatalErrors, err = cl.GetFatalErrors(ctx, wfName, workflowId, failedExecutionId, "")
		require.NoError(t, err)
		return len(fatalErrors) == 1 && fatalErrors[0].WorkflowState.ExecutionId == failedExecutionId
	}, 3*time.Second, 100*time.Millisecond)

	//attempt the retry here!!!
	wfState := fatalErrors[0].WorkflowState

	decodedVars := model.NewVars()
	err = decodedVars.Decode(ctx, wfState.Vars)
	require.NoError(t, err)

	decodedVars.SetBool("blowUp", false)
	encodedVars, err := decodedVars.Encode(ctx)
	require.NoError(t, err)

	err = cl.Retry(ctx, wfState.ProcessInstanceId, wfState.ElementId, encodedVars)
	require.NoError(t, err)

	//assert receipt of completion msg for both original and retried executions
	support.WaitForChan(t, completionChan, time.Second*20, func(ele processCompletion) {
		pId, err := ele.vars.GetInt64("pId")
		assert.NoError(t, err)
		assert.Equal(t, pId1, pId)
	})

	support.WaitForChan(t, completionChan, time.Second*20, func(ele processCompletion) {
		pId, err := ele.vars.GetInt64("pId")
		assert.NoError(t, err)
		assert.Equal(t, pId2, pId)
	})

	//assert non existence of FatalError
	assert.Eventually(t, func() bool {
		fatalErrors, err := cl.GetFatalErrors(ctx, wfName, workflowId, failedExecutionId, "")
		require.NoError(t, err)
		return len(fatalErrors) == 0
	}, 3*time.Second, 100*time.Millisecond)
}

func TestRetryFatalErroredProcessFromPriorElement(t *testing.T) {
	t.Parallel()
	pId1 := int64(1)
	pId2 := int64(2)

	runFatallyErroringProcessesFn := func(ctx context.Context, ns string, cl *client.Client) (chan processCompletion, string, []*model.FatalError) { // run executions
		newVars1 := model.NewVars()
		newVars1.SetBool("blowUp", false)
		newVars1.SetInt64("pId", int64(pId1))

		newVars2 := model.NewVars()
		newVars2.SetBool("blowUp", true)
		newVars2.SetInt64("pId", int64(pId2))

		execParams := []executionParams{
			{startVars: newVars1, expectingFailure: false},
			{startVars: newVars2, expectingFailure: true},
		}

		fatalErrorHandlerDef := fatalErrorHandledHandlerDef{test: t, fatalErr: make(chan *model.FatalError)}

		wfName := "ExclusiveGatewayRetry"

		workflowId, executionIds, completionChan := runExecutions(ctx, fatalErrorHandlerDef, cl, ns, execParams, bpmnDetails{
			processId: "Process_0ljss15",
			bpmnFile:  "../../../testdata/gateway-exclusive-out-and-in-test-retry-from.bpmn",
			wfName:    wfName,
		}, map[string]task.ServiceFn{
			"../gateway/gateway_test_stage0.yaml": fatalErrorHandlerDef.excStage0,
			"../gateway/gateway_test_stage1.yaml": fatalErrorHandlerDef.excStage1,
			"../gateway/gateway_test_stage2.yaml": fatalErrorHandlerDef.excStage2,
			"../gateway/gateway_test_stage3.yaml": fatalErrorHandlerDef.excStage3,
		})

		assert.Eventually(t, func() bool {
			successExecutionId := executionIds[0]
			fatalErrors, err := cl.GetFatalErrors(ctx, wfName, workflowId, successExecutionId, "")
			require.NoError(t, err)
			return len(fatalErrors) == 0
		}, 3*time.Second, 100*time.Millisecond)

		var fatalErrors []*model.FatalError
		failedExecutionId := executionIds[1]
		require.Eventually(t, func() bool {
			var err error
			fatalErrors, err = cl.GetFatalErrors(ctx, wfName, workflowId, failedExecutionId, "")
			require.NoError(t, err)
			//TODO parameterise this
			return len(fatalErrors) == 1 && fatalErrors[0].WorkflowState.ExecutionId == failedExecutionId
		}, 3*time.Second, 100*time.Millisecond)

		return completionChan, wfName, fatalErrors
	}

	type retryParams struct {
		processInstanceId string
		elementId         string
		vars              []byte
		priorElementId    string
	}

	retryFatallyErroredProcessFn := func(ctx context.Context, cl *client.Client, fatalErrors []*model.FatalError, buildRetryParamsFn func(context.Context, []*model.FatalError) retryParams, assertExpectedEerrorFn func(err error)) { //retry failures
		//attempt the retry here!!!
		params := buildRetryParamsFn(ctx, fatalErrors)
		err := cl.RetryFromPriorElement(ctx, params.processInstanceId, params.elementId, params.vars, params.priorElementId)
		assertExpectedEerrorFn(err)
	}

	assertExpectedRetryResult := func(ctx context.Context, cl *client.Client, completionChan chan processCompletion, wfName string) { //assert expected retry result
		//assert receipt of completion msg for both original and retried executions
		support.WaitForChan(t, completionChan, time.Second*20, func(ele processCompletion) {
			pId, err := ele.vars.GetInt64("pId")
			assert.NoError(t, err)
			assert.Equal(t, pId1, pId)
			//assert vals from stage 2
			value1, err := ele.vars.GetInt64("value1")
			assert.NoError(t, err)
			assert.Equal(t, int64(11), value1)
			value2, err := ele.vars.GetInt64("value2")
			assert.NoError(t, err)
			assert.Equal(t, int64(22), value2)
		})

		support.WaitForChan(t, completionChan, time.Second*20, func(ele processCompletion) {
			pId, err := ele.vars.GetInt64("pId")
			assert.NoError(t, err)
			assert.Equal(t, pId2, pId)
			//assert vals from stage 1
			value1, err := ele.vars.GetInt64("value1")
			assert.NoError(t, err)
			assert.Equal(t, int64(1), value1)
			value2, err := ele.vars.GetInt64("value2")
			assert.NoError(t, err)
			assert.Equal(t, int64(2), value2)
		})

		//assert non existence of FatalError
		assert.Eventually(t, func() bool {
			fatalErrors, err := cl.GetFatalErrors(ctx, wfName, "", "", "")
			require.NoError(t, err)
			return len(fatalErrors) == 0
		}, 3*time.Second, 100*time.Millisecond)
	}

	scenarios := map[string]struct {
		buildRetryParams       func(ctx context.Context, fatalErrors []*model.FatalError) retryParams
		assertExpectedRetryErr func(err error)
		expectRetrySuccess     bool
	}{
		"successfully retries fatally errored process from prior element": {
			buildRetryParams: func(ctx context.Context, fatalErrors []*model.FatalError) retryParams {
				wfState := fatalErrors[0].WorkflowState

				decodedVars := model.NewVars()
				err := decodedVars.Decode(ctx, wfState.Vars)
				require.NoError(t, err)

				decodedVars.SetBool("blowUp", false)
				encodedVars, err := decodedVars.Encode(ctx)
				require.NoError(t, err)

				return retryParams{processInstanceId: wfState.ProcessInstanceId, elementId: wfState.ElementId, vars: encodedVars, priorElementId: "Activity_1vtux4u"}
			},
			assertExpectedRetryErr: func(err error) { require.NoError(t, err) },
			expectRetrySuccess:     true,
		},
		"errors when there is no fatal error found for element": {
			buildRetryParams: func(_ context.Context, fatalErrors []*model.FatalError) retryParams {
				wfState := fatalErrors[0].WorkflowState
				elementId := "invalidElementId"
				return retryParams{processInstanceId: wfState.ProcessInstanceId, elementId: elementId, vars: wfState.Vars, priorElementId: "Activity_1vtux4u"}
			},
			assertExpectedRetryErr: func(err error) { require.ErrorContains(t, err, "unable to find fatal errored workflow to retry") },
			expectRetrySuccess:     false,
		},
		"errors when attempting to retry from a non executed element": {
			buildRetryParams: func(_ context.Context, fatalErrors []*model.FatalError) retryParams {
				wfState := fatalErrors[0].WorkflowState
				invalidPriorElement := "Event_0gzaaxbzz"
				return retryParams{processInstanceId: wfState.ProcessInstanceId, elementId: wfState.ElementId, vars: wfState.Vars, priorElementId: invalidPriorElement}
			},
			assertExpectedRetryErr: func(err error) { require.ErrorContains(t, err, "cannot find prior element to retry") },
			expectRetrySuccess:     false,
		},
	}

	for name, testParams := range scenarios {
		t.Run(name, func(t *testing.T) {
			t.Parallel()
			// Create a starting context
			ctx := context.Background()

			// Dial shar
			ns := ksuid.New().String()
			cl := client.New(client.WithEphemeralStorage(), client.WithConcurrency(10), client.WithNamespace(ns))
			err := cl.Dial(ctx, tst.NatsURL)
			require.NoError(t, err)

			completionChan, wfName, fatalErrors := runFatallyErroringProcessesFn(ctx, ns, cl)
			retryFatallyErroredProcessFn(ctx, cl, fatalErrors, testParams.buildRetryParams, testParams.assertExpectedRetryErr)
			if testParams.expectRetrySuccess {
				assertExpectedRetryResult(ctx, cl, completionChan, wfName)
			}
		})
	}

}

//TODO retry from an element within a branch of the gateway

type processCompletion struct {
	vars model.Vars
}

type bpmnDetails struct {
	processId string
	bpmnFile  string
	wfName    string
}

func runExecutions(ctx context.Context, d fatalErrorHandledHandlerDef, cl *client.Client, ns string, executionParams []executionParams, bpmnDetails bpmnDetails, svcTasks map[string]task.ServiceFn) (string, map[int]string, chan processCompletion) {
	var err error
	// Register service tasks
	for svcTaskSpecFile, svcTaskFn := range svcTasks {
		_, err = support.RegisterTaskYamlFile(ctx, cl, svcTaskSpecFile, svcTaskFn)
		require.NoError(d.test, err)
	}
	processId := bpmnDetails.processId

	processCompletionChannel := make(chan processCompletion)
	err = cl.RegisterProcessComplete(processId, func(ctx context.Context, vars model.Vars, wfError *model.Error, endState model.CancellationState) {
		processCompletionChannel <- processCompletion{vars}
	})
	require.NoError(d.test, err)

	cl.RegisterFatalErrorListener(processId, func(ctx context.Context, fatalError *model.FatalError) {
		d.fatalErr <- fatalError
	})

	// Load BPMN workflow
	b, err := os.ReadFile(bpmnDetails.bpmnFile)
	require.NoError(d.test, err)
	wfName := bpmnDetails.wfName
	if _, err := cl.LoadBPMNWorkflowFromBytes(ctx, client.LoadWorkflowParams{Name: wfName, WorkflowBPMN: b}); err != nil {
		panic(err)
	}
	workflowId := getWorkflowIdFrom(d.test, ctx, cl, bpmnDetails.wfName)

	executionIds := make(map[int]string, len(executionParams))

	for i, executionParam := range executionParams {
		// Launch the workflow
		executionId, _, err := cl.LaunchProcess(ctx, client.LaunchParams{ProcessID: processId, Vars: executionParam.startVars})
		require.NoError(d.test, err)

		executionIds[i] = executionId

		// Listen for service tasks
		go func() {
			err := cl.Listen(ctx)
			require.NoError(d.test, err)
		}()

		if executionParam.expectingFailure {
			support.WaitForChan(d.test, d.fatalErr, 20*time.Second, func(fatalErr *model.FatalError) {
				require.Equal(d.test, executionId, fatalErr.WorkflowState.ExecutionId)
			})

			expectedFatalErrorKey := fmt.Sprintf("%s.%s.%s.>", bpmnDetails.wfName, workflowId, executionId)
			tst.AssertExpectedKVKey(ns, messages.KvFatalError, expectedFatalErrorKey, 20*time.Second, d.test)
		}
	}
	return workflowId, executionIds, processCompletionChannel
}

func TestFatalErrorPersistedWhenRetriesAreExhaustedAndErrorActionIsPause(t *testing.T) {
	t.Parallel()

	// Create a starting context
	ctx := context.Background()

	// Dial shar
	ns := ksuid.New().String()
	cl := client.New(client.WithEphemeralStorage(), client.WithConcurrency(10), client.WithNamespace(ns))
	err := cl.Dial(ctx, tst.NatsURL)
	require.NoError(t, err)

	d := fatalErrorHandledHandlerDef{test: t, fatalErr: make(chan *model.FatalError)}

	// Register service tasks
	_, err = support.RegisterTaskYamlFile(ctx, cl, "../simple/simple_test_pause_on_retry_exhausted.yaml", d.willResultInRetryExhaustion)
	require.NoError(t, err)

	// Load BPMN workflow
	b, err := os.ReadFile("../../../testdata/simple-workflow.bpmn")
	require.NoError(t, err)
	wfName := "TestHandleFatalError"
	if _, err := cl.LoadBPMNWorkflowFromBytes(ctx, client.LoadWorkflowParams{Name: wfName, WorkflowBPMN: b}); err != nil {
		panic(err)
	}

	processID := "SimpleProcess"
	cl.RegisterFatalErrorListener(processID, func(ctx context.Context, fatalError *model.FatalError) {
		d.fatalErr <- fatalError
	})

	// Launch the workflow
	executionId, _, err := cl.LaunchProcess(ctx, client.LaunchParams{ProcessID: processID, Vars: model.NewVars()})
	require.NoError(t, err)

	workflowId := getWorkflowIdFrom(t, ctx, cl, wfName)

	// Listen for service tasks
	go func() {
		err := cl.Listen(ctx)
		require.NoError(t, err)
	}()

	// wait for the fatal err to appear
	support.WaitForChan(t, d.fatalErr, 20*time.Second, func(ele *model.FatalError) {
		require.Equal(t, executionId, ele.WorkflowState.ExecutionId)
	})

	expectedFatalErrorKey := fmt.Sprintf("%s.%s.%s.>", wfName, workflowId, executionId)
	tst.AssertExpectedKVKey(ns, messages.KvFatalError, expectedFatalErrorKey, 20*time.Second, t)
}

type fatalErrorHandledHandlerDef struct {
	test     *testing.T
	fatalErr chan *model.FatalError
}

func willPanicAndCauseWorkflowFatalError(_ context.Context, _ task.JobClient, v model.Vars) (model.Vars, error) {
	// panic and cause a WorkflowFatalError
	if true {
		panic(fmt.Errorf("BOOM, cause an ErrWorkflowFatal to be thrown"))
	}
	retVars := model.NewVars()
	retVars.SetBool("success", true)
	retVars.SetInt64("myVar", 69) // nolint:ireturn
	return retVars, nil
}

func (d *fatalErrorHandledHandlerDef) willResultInRetryExhaustion(ctx context.Context, jobClient task.JobClient, vars model.Vars) (model.Vars, error) {
	return nil, fmt.Errorf("test error: %w", errors.New("I will cause retry exhaustion"))
}

func willConditionallyPanicDependingOnVar(_ context.Context, _ task.JobClient, vars model.Vars) (model.Vars, error) {
	slog.Info("in willConditionallyPanicDependingOnVar", "vars", vars)
	blowUp, _ := vars.GetBool("blowUp")
	if blowUp {
		panic(fmt.Errorf("BLOW UP, cause an ErrWorkflowFatal to be thrown"))
	}

	return vars, nil
}

func (d *fatalErrorHandledHandlerDef) excStage0(_ context.Context, _ task.JobClient, vars model.Vars) (model.Vars, error) {
	fmt.Println("Stage 0")
	newVars := model.NewVars()
	pId, err := vars.GetInt64("pId")
	require.NoError(d.test, err)
	newVars.SetBool("takeBranch", pId%2 == 0)
	return newVars, nil
}

func (d *fatalErrorHandledHandlerDef) excStage1(_ context.Context, _ task.JobClient, _ model.Vars) (model.Vars, error) {
	fmt.Println("Stage 1")
	newVars := model.NewVars()
	newVars.SetInt64("value1", 1)
	newVars.SetInt64("value2", 2)
	return newVars, nil
}

func (d *fatalErrorHandledHandlerDef) excStage2(_ context.Context, _ task.JobClient, _ model.Vars) (model.Vars, error) {
	fmt.Println("Stage 2")
	newVars := model.NewVars()
	newVars.SetInt64("value1", 11)
	newVars.SetInt64("value2", 22)
	return newVars, nil
}

func (d *fatalErrorHandledHandlerDef) excStage3(_ context.Context, _ task.JobClient, vars model.Vars) (model.Vars, error) {
	fmt.Println("Stage 3")
	blowUp, err := vars.GetBool("blowUp")
	assert.NoError(d.test, err)

	if blowUp {
		panic(fmt.Errorf("fail this service task with fatal err"))
	}

	newVars := model.NewVars()
	newVars.SetInt64("value3", 3)
	return newVars, nil
}
