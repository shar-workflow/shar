package intTest

import (
	"context"
	"fmt"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/shar-workflow/shar/client"
	"gitlab.com/shar-workflow/shar/client/task"
	support "gitlab.com/shar-workflow/shar/internal/integration-support"
	"gitlab.com/shar-workflow/shar/model"
	"os"
	"sync"
	"testing"
	"time"
)

const pidVarName = "pId"

func TestLongRunningServiceTaskShouldOnlyBeExecutedOnce(t *testing.T) {
	// Create a starting context
	ctx := context.Background()

	// Dial shar
	ns := ksuid.New().String()
	cl := client.New(client.WithEphemeralStorage(), client.WithConcurrency(10), client.WithNamespace(ns))

	err := cl.Dial(ctx, tst.NatsURL)
	require.NoError(t, err)

	svcTaskConsumerAckTimeout := time.Second * 2
	svcTaskExecutionTime := svcTaskConsumerAckTimeout * 3

	// Register a service task
	d := &testLongRunningTaskHandlerDef{finished: make(chan struct{}), svcTaskExecTime: svcTaskExecutionTime, svcTaskInvokationCount: map[string]int{}, t: t}

	uid, err := support.RegisterTaskYamlFile(ctx, cl, "../simple/simple_test.yaml", d.longRunningSvcTask)
	require.NoError(t, err)

	err = cl.RegisterProcessComplete("SimpleProcess", d.processEnd)
	require.NoError(t, err)

	// Load BPMN workflow
	b, err := os.ReadFile("../../../testdata/simple-workflow.bpmn")
	require.NoError(t, err)

	_, err = cl.LoadBPMNWorkflowFromBytes(ctx, client.LoadWorkflowParams{Name: "SimpleWorkflowTest", WorkflowBPMN: b})
	require.NoError(t, err)

	svcTaskConsumerName := fmt.Sprintf("ServiceTask_%s_%s", ns, uid)
	//reduce ack wait to make test complete in reasonable time
	setSvcTaskConsumerAckWait(ctx, t, svcTaskConsumerAckTimeout, svcTaskConsumerName)

	// Launch the workflow
	pid1 := "pid1"
	startVars, err := model.NewVarsFromMap(map[string]any{pidVarName: pid1})
	require.NoError(t, err)
	_, _, err = cl.LaunchProcess(ctx, client.LaunchParams{ProcessID: "SimpleProcess", Vars: startVars})
	require.NoError(t, err)

	// Listen for service tasks
	go func() {
		err := cl.Listen(ctx)
		require.NoError(t, err)
	}()

	support.WaitForChan(t, d.finished, 20*time.Second)
	tst.AssertCleanKV(ns, t, 60*time.Second)
	assert.Equal(t, 1, d.svcTaskInvokationCount[pid1])
}

func setSvcTaskConsumerAckWait(ctx context.Context, t *testing.T, svcTaskConsumerAckTimeout time.Duration, svcTaskConsumerName string) {
	js, err := tst.GetJetstream()
	require.NoError(t, err)

	stream, err := js.Stream(ctx, "WORKFLOW")
	require.NoError(t, err)
	consumer, err := stream.Consumer(ctx, svcTaskConsumerName)
	require.NoError(t, err)
	info, err := consumer.Info(ctx)
	require.NoError(t, err)
	conf := info.Config
	conf.AckWait = svcTaskConsumerAckTimeout

	_, err = js.UpdateConsumer(ctx, "WORKFLOW", conf)
	require.NoError(t, err)
}

type testLongRunningTaskHandlerDef struct {
	finished               chan struct{}
	svcTaskExecTime        time.Duration
	svcTaskInvokationCount map[string]int
	lock                   sync.Mutex
	t                      *testing.T
}

func (h *testLongRunningTaskHandlerDef) longRunningSvcTask(_ context.Context, _ task.JobClient, vars model.Vars) (model.Vars, error) {
	pid, err := vars.GetString(pidVarName)
	require.NoError(h.t, err)

	h.lock.Lock()
	invokeCount, ok := h.svcTaskInvokationCount[pid]
	if !ok {
		invokeCount = 1
	} else {
		invokeCount++
	}
	h.svcTaskInvokationCount[pid] = invokeCount
	h.lock.Unlock()

	<-time.After(h.svcTaskExecTime)
	return vars, nil
}

func (h *testLongRunningTaskHandlerDef) processEnd(_ context.Context, _ model.Vars, _ *model.Error, _ model.CancellationState) {
	close(h.finished)
}
