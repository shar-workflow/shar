package waitgroup

import (
	"context"
	"fmt"
	"github.com/nats-io/nats.go"
	"github.com/nats-io/nats.go/jetstream"
	"github.com/stretchr/testify/assert"
	"gitlab.com/shar-workflow/shar/common"
	"strings"
	"testing"
	"time"
)

func TestNatsWaitgroup(t *testing.T) {
	t.Parallel()
	// Create a starting context
	ctx := context.Background()

	nc, err := nats.Connect(tst.NatsURL)
	assert.NoError(t, err)
	js, err := jetstream.New(nc)
	assert.NoError(t, err)

	strCfg := jetstream.StreamConfig{
		Name:     "TESTBED1",
		Subjects: []string{"TESTBED1.Message.>"},
		Storage:  jetstream.MemoryStorage,
	}
	_, err = js.CreateOrUpdateStream(ctx, strCfg)
	if err != nil {
		panic(fmt.Errorf("create/update stream: %w", err))
	}

	type kv struct {
		key   string
		value uint64
	}
	res := make(chan kv)

	ccx, err := common.CreateOrUpdateNatsWaitGroup(ctx, js, "TestConsumer1", "TESTBED1", "TESTBED1.Message", func(js jetstream.Stream, msg jetstream.Msg, n uint64) (bool, error) {
		if n > 1 {
			res <- kv{key: strings.Split(msg.Subject(), ".")[2], value: n} // nolint
			return true, nil
		}
		return false, nil
	},
		common.WithMemoryStorage(),
		common.WithLockTimeout(30*time.Second),
		common.WithCompletionBuffer("CompletedMessages", 24*time.Hour*90),
	)
	if err != nil {
		panic(err)
	}
	defer ccx.Stop()

	msg := &nats.Msg{
		Subject: "TESTBED1.Message.EFG",
	}
	_, err = js.PublishMsg(ctx, msg)
	if err != nil {
		panic(err)
	}
	msg = &nats.Msg{
		Subject: "TESTBED1.Message.ABC",
	}
	_, err = js.PublishMsg(ctx, msg)
	if err != nil {
		panic(err)
	}

	time.Sleep(2 * time.Second)

	_, err = js.PublishMsg(ctx, msg)
	if err != nil {
		panic(err)
	}
	r := <-res
	assert.Equal(t, r.key, "ABC")
	assert.Equal(t, r.value, uint64(2))
}
