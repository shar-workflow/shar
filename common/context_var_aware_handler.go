package common

import (
	"context"
	"fmt"
	"log/slog"
)

// ContextVarAwareHandler is a slog handler that looks for the specified context var keys and then adds
// them as log record attributes
type ContextVarAwareHandler struct {
	handler        slog.Handler
	contextVarKeys map[any]struct{}
}

// NewContextVarAwareHandler create new instance of ContextVarAwareHandler
func NewContextVarAwareHandler(h slog.Handler, contextVarKeys map[any]struct{}) (slog.Handler, error) {
	if h == nil || contextVarKeys == nil || len(contextVarKeys) == 0 {
		return nil, fmt.Errorf("handler and contextVarKeys must be non nil")
	}

	return &ContextVarAwareHandler{
		handler:        h,
		contextVarKeys: contextVarKeys,
	}, nil

}

// Enabled implementation of slog.Handler interface
func (cvah *ContextVarAwareHandler) Enabled(ctx context.Context, level slog.Level) bool {
	return cvah.handler.Enabled(ctx, level)
}

// Handle implementation of slog.Handler interface
func (cvah *ContextVarAwareHandler) Handle(ctx context.Context, r slog.Record) error {
	for ctxVarKey, _ := range cvah.contextVarKeys {
		ctxVarVal := ctx.Value(ctxVarKey)
		if ctxVarVal != nil {
			r.Add(fmt.Sprintf("%v", ctxVarKey), ctxVarVal)
		}
	}

	if err := cvah.handler.Handle(ctx, r); err != nil {
		return fmt.Errorf("context var aware handler handle: %w", err)
	}
	return nil
}

// WithAttrs implementation of slog.Handler interface
func (cvah *ContextVarAwareHandler) WithAttrs(attrs []slog.Attr) slog.Handler {
	return &ContextVarAwareHandler{
		handler:        cvah.handler.WithAttrs(attrs),
		contextVarKeys: cvah.contextVarKeys,
	}
}

// WithGroup implementation of slog.Handler interface
func (cvah *ContextVarAwareHandler) WithGroup(name string) slog.Handler {
	return &ContextVarAwareHandler{
		handler:        cvah.handler.WithGroup(name),
		contextVarKeys: cvah.contextVarKeys,
	}
}
