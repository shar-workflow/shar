package common

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"log/slog"
	"testing"
	"time"
)

type contextKeyTest string

func TestContextVarAwareHandler(t *testing.T) {
	ctxAttr1Name := "ctxAttr1"
	ctxAttr1Val := "ctxAttr1Val"

	attrAName := "attrA"
	attrAVal := "attrAVal"

	ctx := context.Background()
	ctxWithAttr1 := context.WithValue(ctx, contextKeyTest(ctxAttr1Name), ctxAttr1Val)

	ctxAttr2Name := "ctxAttr2"
	ctxAttr2Val := "ctxAttr2Val"
	ctxWithAttr2 := context.WithValue(ctxWithAttr1, contextKeyTest(ctxAttr2Name), ctxAttr2Val)

	groupName := "GroupNameOne"

	initHandler := func(buf *bytes.Buffer) slog.Handler {
		jsonHandler := slog.NewJSONHandler(buf, nil)

		contextVarKeys := map[any]struct{}{contextKeyTest(ctxAttr1Name): {}}
		contextVarAwareHandler, err := NewContextVarAwareHandler(jsonHandler, contextVarKeys)
		assert.NoError(t, err)
		return contextVarAwareHandler.WithAttrs([]slog.Attr{slog.String(attrAName, attrAVal)})
	}

	for testName, testParams := range map[string]struct {
		newHandler func(buf *bytes.Buffer) slog.Handler
		assertions func(jsonRecord map[string]any)
	}{
		"handles context var as attributes": {
			newHandler: initHandler,
			assertions: func(jsonLogRecord map[string]any) {
				assert.Contains(t, jsonLogRecord, attrAName)
				assert.Equal(t, attrAVal, jsonLogRecord[attrAName])

				assert.Contains(t, jsonLogRecord, ctxAttr1Name)
				assert.Equal(t, ctxAttr1Val, jsonLogRecord[ctxAttr1Name])
				assert.NotContains(t, ctxAttr2Name, jsonLogRecord)
			},
		},
		"handles context var as attributes with group": {
			newHandler: func(b *bytes.Buffer) slog.Handler { return initHandler(b).WithGroup(groupName) },
			assertions: func(jsonLogRecord map[string]any) {
				assert.Contains(t, jsonLogRecord, attrAName)
				assert.Equal(t, attrAVal, jsonLogRecord[attrAName])

				assert.Contains(t, jsonLogRecord, groupName)
				group := jsonLogRecord[groupName].(map[string]any)
				assert.Contains(t, group, ctxAttr1Name)
				assert.Equal(t, ctxAttr1Val, group[ctxAttr1Name])

				assert.NotContains(t, jsonLogRecord, ctxAttr2Name)
			},
		},
	} {
		t.Run(testName, func(t *testing.T) {
			record := slog.NewRecord(time.Now(), slog.LevelInfo, "the msg", 0)
			buf := &bytes.Buffer{}
			err := testParams.newHandler(buf).Handle(ctxWithAttr2, record)
			assert.NoError(t, err)

			jsonLogRecord := make(map[string]any)
			err = json.Unmarshal(buf.Bytes(), &jsonLogRecord)
			assert.NoError(t, err)

			testParams.assertions(jsonLogRecord)
		})
	}

}
