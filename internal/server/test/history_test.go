package test

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/shar-workflow/shar/internal/server/workflow"
	"gitlab.com/shar-workflow/shar/model"
	"gitlab.com/shar-workflow/shar/server/server"
	"gitlab.com/shar-workflow/shar/server/services/natz"
	"testing"
)

func TestHistoryIsRetrievableByElementId(t *testing.T) {
	natsConn, err := server.ConnectNats("", tst.NatsURL, nil, true)
	require.NoError(t, err)

	natsService, err := natz.NewNatsService(natsConn)
	require.NoError(t, err)

	historyOperations := workflow.NewHistoryOperations(natsService)

	ctx := context.Background()

	//record some activity history here
	processInstanceId := "processInstanceId1"
	elementId1 := "elementId1"
	elementId2 := "elementId2"
	elementId3 := "elementId3"
	id1 := "trackingId1"
	id2 := "trackingId2"
	id3 := "trackingId3"
	trackingElementId := map[string]string{
		id1: elementId1,
		id2: elementId2,
		id3: elementId3,
	}
	wfStatesForElements := []*model.WorkflowState{
		{ProcessInstanceId: processInstanceId, Id: []string{id1}, ElementId: elementId1},
		{ProcessInstanceId: processInstanceId, Id: []string{id2}, ElementId: elementId2},
		{ProcessInstanceId: processInstanceId, Id: []string{id3}, ElementId: elementId3},
	}

	for _, wfState := range wfStatesForElements {
		err = historyOperations.RecordHistoryActivityExecute(ctx, wfState)
		require.NoError(t, err)
	}

	for trackingId, elementId := range trackingElementId {
		//call the new method with element Id
		processHistoryEntry, err := historyOperations.GetProcessHistoryItemForElement(ctx, processInstanceId, elementId)
		require.NoError(t, err)
		//assert we have the expected history entry
		assert.Equal(t, trackingId, processHistoryEntry.WorkflowState.Id[0])
	}
}
